/********************************************
/
/ A demo theme for a two-pass CRT shader
/
/ 20/09/2018 - Luke Jones
/
/ See headers in shaders for credits
/
********************************************/

class UserConfig {	
	</ label="Screen Rotate", help="Swap X/Y dimensions for rotated screens",
	options="Yes,No" />
	rotated="No";
}

// Globals
list_X <- 10;	
list_Y <- 0;
title_X <- (fe.layout.width / 150);
title_Y <- 0;
titleSize <- 42;

local layoutSettings = fe.get_config();

switch (layoutSettings["rotated"]){
	case "No": 
        fe.layout.width = ScreenWidth;
        fe.layout.height = ScreenHeight;
        title_Y = (fe.layout.height / 1.10);
        fe.layout.orient = RotateScreen.None;
        break;
	case "Yes" :
        fe.layout.width = ScreenHeight;
        fe.layout.height = ScreenWidth;
        title_Y = (fe.layout.height / 1.08);
    	fe.layout.orient = RotateScreen.Right;
        break;
};

// variables common across frags
local crt_gamma = 2.4;
local monitor_gamma = 2.2;

// Shader Setup
local shader_gaus_h = null;
shader_gaus_h=fe.add_shader(
    Shader.Fragment,
    "shaders/gaussian-horiz.fs");
shader_gaus_h.set_param("CRTgamma", crt_gamma);          // gamma of simulated CRT
shader_gaus_h.set_param("monitorgamma", monitor_gamma);      // gamma of display monitor (typically 2.2 is correct)
shader_gaus_h.set_texture_param("source");

local shader_gaus_v = null;
shader_gaus_v=fe.add_shader(
    Shader.Fragment,
    "shaders/gaussian-vert.fs");
shader_gaus_v.set_param("CRTgamma", crt_gamma);          // gamma of simulated CRT
shader_gaus_v.set_param("monitorgamma", monitor_gamma);      // gamma of display monitor (typically 2.2 is correct)
shader_gaus_v.set_texture_param("source");

local shader_combine = null;
shader_combine=fe.add_shader(
    Shader.Fragment,
    "shaders/combine.fs");

local shader_geom = null;
shader_geom=fe.add_shader(
    Shader.VertexAndFragment,
    "shaders/crt-geom.vs",
    "shaders/crt-geom.fs");
shader_geom.set_param("CRTgamma", crt_gamma);          // gamma of simulated CRT
shader_geom.set_param("monitorgamma", monitor_gamma);      // gamma of display monitor (typically 2.2 is correct)
shader_geom.set_param("overscan", 0.99, 0.99);   // overscan (e.g. 1.02 for 2% overscan)
shader_geom.set_param("aspect", 1.0, 0.75);      // aspect ratio
shader_geom.set_param("d", 1.3);                 // distance from viewer
shader_geom.set_param("R", 2.5);                 // radius of curvature - 2.0 to 3.0?
shader_geom.set_param("cornersize", 0.02);       // size of curved corners
shader_geom.set_param("cornersmooth", 80);       // border smoothness parameter
                                                // decrease if borders are too aliased
shader_geom.set_texture_param("texture");

// Video overlay.
local surface1 = fe.add_surface(fe.layout.width, fe.layout.height);
local surface2 = surface1.add_surface(surface1.width, surface1.height);
local surface3 = surface2.add_surface(surface2.width, surface2.height);
local final_surf = surface3.add_surface(surface3.width, surface3.height);

surface1.shader = shader_geom;
surface2.shader = shader_gaus_h;
surface3.shader = shader_gaus_v;
final_surf.shader = shader_combine;

// Setup the combine - must be current frame + previous frame
// Not sure this is actually possible to do with Attract Mode
final_surf.shader.set_texture_param("source1", surface3); // current
final_surf.shader.set_texture_param("source2", surface1); // previous

local video = surface1.add_artwork("snap", 0, 0, surface1.width, surface1.height);
video.set_rgb (255,255,255);
video.preserve_aspect_ratio = true;

// Transitions
fe.add_transition_callback( "new_transitions" );

function new_transitions( ttype, var, ttime ) {
	switch ( ttype )
	{
	case Transition.ToNewList:	
	case Transition.FromOldSelection:
	case Transition.ToNewSelection:
		video.width = surface1.subimg_width;
        video.height = surface1.subimg_height;
        // Division of source dimensions controls scanline amount
        surface1.shader.set_param("inputSize", video.width, video.height);
        // Division of dimensions here controls sharpness
        surface1.shader.set_param("targetSize", video.width, video.height);
        //
        surface1.shader.set_param("outputSize", surface1.width/2, surface1.height/2); // size of mask

        surface2.shader.set_param("inputSize", video.width, video.height);
        surface3.shader.set_param("inputSize", video.width, video.height);
		break;
    }
	return false;
}


// List Title
romListSurf <- fe.add_surface ( fe.layout.width, 200);
romList <- romListSurf.add_text( "[ListFilterName]", 0, 5, fe.layout.width - 5, 82 );
romList.align = Align.Centre;
romList.style = Style.Bold;

// List Position
listPosition <- romListSurf.add_text( "[ListEntry]/[ListSize]", 0, 100, fe.layout.width, 32 );
listPosition.align = Align.Centre;
listPosition.style = Style.Bold;

// Game title block
gameTitle <- fe.add_text( "[Title] ([Year])", (title_X - 2), (title_Y - 2), fe.layout.width, titleSize );
gameTitle.align = Align.Centre;
gameTitle.style = Style.Bold;
