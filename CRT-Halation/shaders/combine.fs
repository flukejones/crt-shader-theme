#version 130
#define halation 0.35

uniform sampler2D source1;
uniform sampler2D source2;
varying vec2 texCoord;

void main() {
    vec4 image = pow(texture2D(source1, texCoord).rgba, vec4(2.2));
    vec4 previous = pow(texture2D(source2, texCoord).rgba, vec4(2.2));
    vec4 combined = mix(previous, image, 1.0 - halation);

    gl_FragColor = pow(combined, vec4(1.0 / 2.2));
}
